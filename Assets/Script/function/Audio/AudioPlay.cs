using UnityEngine;

public class AudioPlay : MonoBehaviour
{
    public string audioName;

    public bool isLoop;

    [Tooltip("BGM/Voice/Effect/UI")] public string chSource;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.Instance.AudioPlay(chSource, audioName, isLoop);
    }
}