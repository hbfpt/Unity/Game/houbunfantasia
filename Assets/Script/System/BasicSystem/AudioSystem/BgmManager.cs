using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;
using System.Linq;

public class BgmManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private float delayTime = 5f; // 停留时间

    [Header("CSV Data")]
    [SerializeField] private TextAsset bgmListCsvPath; // csv文件路径

    private List<string> bgmList = new List<string>(); // BGM列表
    private int _currentBgmIndex = 0; // 当前播放的BGM索引

    private AudioManager _audioManager; // AudioManager的引用
    private AudioSource _bgmAudioSource; // 音频源

    private bool _isPlaying; // 当前是否在播放BGM

    // 初始化
    private void Start()
    {
        _audioManager = AudioManager.Instance;
        _bgmAudioSource = _audioManager.bgmSource;

        // 从csv文件中读取BGM列表
        string[] csvData = bgmListCsvPath.text.Split('\n');
        foreach (string row in csvData.Skip(1))
        {
            string[] rowData = row.Trim().Split(',');
            bgmList.Add(rowData[0]);
        }

        // 初始化BGM
        PlayBgm();
    }

    // 播放下一首BGM
    private IEnumerator PlayNextBgm()
    {
        // 获取下一首BGM的索引
        int nextIndex = GetNextBgmIndex();

        // 停留一段时间
        yield return new WaitForSeconds(delayTime);

        // 切换BGM
        _audioManager.AudioPlay("BGM", bgmList[nextIndex], false);
        _bgmAudioSource.Play();

        // 更新当前BGM索引和是否正在播放的状态
        _currentBgmIndex = nextIndex;
        _isPlaying = true;

        yield return null;
    }

    // 获取下一首BGM的索引
    private int GetNextBgmIndex()
    {
        int nextIndex = Random.Range(0, bgmList.Count);
        while (nextIndex == _currentBgmIndex)
        {
            nextIndex = Random.Range(0, bgmList.Count);
        }
        return nextIndex;
    }

    // 播放BGM
    private void PlayBgm()
    {
        _audioManager.AudioPlay("BGM", bgmList[_currentBgmIndex], false);
        _bgmAudioSource.Play();
        _isPlaying = true;
    }

    // 更新
    private void Update()
    {
        // 如果当前BGM已经播放完了，就开始播放下一首BGM
        if (!_isPlaying && !_bgmAudioSource.isPlaying)
        {
            StartCoroutine(PlayNextBgm());
            return;
        }
        /* 切换BGM测试
        if (Input.GetKeyDown(KeyCode.N))
        {
            StartCoroutine(PlayNextBgm());
            return;
        }
        */
        // 如果当前BGM正在播放，就更新_isPlaying的值
        if (_isPlaying && !_bgmAudioSource.isPlaying)
        {
            _isPlaying = false;
        }
    }
}