using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Audio;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioMixer audioMixer;

    //声明音频源
    public AudioSource bgmSource, effectSource, voiceSource, uiSource;

    [Tooltip("请注意CSV中的格式")] public TextAsset audioCsvPath;

    private Dictionary<string, AudioClip> _audioClips = new Dictionary<string, AudioClip>();

    // 播放音频
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            LoadAudioClips();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private async void LoadAudioClips()
    {
        string[] lines = audioCsvPath.text.Split('\n');

        foreach (string line in lines.Skip(1))
        {
            string[] fields = line.Split(',');
            string audioName = fields[0];
            string audioPath = fields[1];
            bool useAddressables = bool.Parse(fields[2]);

            if (useAddressables)
            {
                AsyncOperationHandle<AudioClip> handle = Addressables.LoadAssetAsync<AudioClip>(audioPath);
                await handle.Task;
                _audioClips.Add(audioName, handle.Result);
            }
            else
            {
                AudioClip clip = Resources.Load<AudioClip>(audioPath);
                _audioClips.Add(audioName, clip);
            }
        }
    }

    private void PlayAudio(AudioSource source, string audioName, bool loop)
    {
        if (_audioClips.TryGetValue(audioName, out AudioClip clip))
        {
            source.clip = clip;
            source.loop = loop;
            source.Play();
        }
        else
        {
            Debug.LogWarning($"Audio clip with name {audioName} not found.");
        }
    }

    public void AudioPlay(string chSource, string audioName, bool loop)
    {
        switch (chSource)
        {
            case "BGM":
                PlayAudio(bgmSource, audioName, loop);
                break;
            case "Voice":
                PlayAudio(voiceSource, audioName, loop);
                break;
            case "Effect":
                PlayAudio(effectSource, audioName, loop);
                break;
            case "UI":
                PlayAudio(uiSource, audioName, loop);
                break;
            default:
                PlayAudio(effectSource, audioName, loop);
                break;
        }
    }

    public void SetVolume(string group, float volume)
    {
        if (volume >= -20f)
        {
            volume = -80f;
        }

        switch (group)
        {
            case "MainVolume":
                audioMixer.SetFloat("MainVolume", volume);
                break;
            case "BGMVolume":
                audioMixer.SetFloat("BGMVolume", volume);
                break;
            case "VoiceVolume":
                audioMixer.SetFloat("VoiceVolume", volume);
                break;
            case "SoundEffectVolume":
                audioMixer.SetFloat("SoundEffectVolume", volume);
                break;
            case "UIVolume":
                audioMixer.SetFloat("UIVolume", volume);
                break;

            default:
                Debug.LogWarning($"Audio group with name {group} not found.");
                break;
        }
    }
}