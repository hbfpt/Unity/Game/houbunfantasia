using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    public Image fadeImage; // 用于淡入淡出的图片组件
    public GameObject nowLoading; // 显示 "Now loading" 的游戏对象
    public Image nowLoadingFadeImage;
    public GameObject loadObject;
    public float fadeTime = 1f; // 淡入淡出的时间
    public float loadingTime = 3f; // 加载完成后等待的时间

    private IEnumerator Start()
    {
        //先关闭提示部分，防止错位
        loadObject.SetActive(false);
        nowLoading.SetActive(true);
        yield return StartCoroutine(FadeOut());
        nowLoading.SetActive(false);
    }

    // 异步加载场景的方法
    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneAsyncCoroutine(sceneName));
    }

    private IEnumerator LoadSceneAsyncCoroutine(string sceneName)
    {
        // 显示 Now loading
        nowLoading.SetActive(true);


        // 淡出
        yield return StartCoroutine(FadeIn());
        //确认提示组件是否打开
        nowLoadingFadeImage.gameObject.SetActive(true);
        loadObject.SetActive(true);
        yield return StartCoroutine(NowLoadingFadeImageFadeOut());

        // 异步加载场景
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        asyncLoad.allowSceneActivation = false;
        // 等待场景加载完成
        while (asyncLoad.progress < 0.9f)
        {
            yield return null;
        }

        Debug.Log("Scene load complete");
        
        // 等待3秒钟
        yield return new WaitForSeconds(loadingTime);
        nowLoadingFadeImage.gameObject.SetActive(true);
        yield return StartCoroutine(NowLoadingFadeImageFadeIn());
        //关闭提示部分，防止错位
        loadObject.SetActive(false);
        asyncLoad.allowSceneActivation = true;
    }

    // 淡出的方法
    private IEnumerator FadeOut()
    {
        // 设置图片透明度为0
        fadeImage.color = new Color(0, 0, 0, 0);

        // 淡出
        float t = 0f;
        while (t < fadeTime)
        {
            t += Time.deltaTime;
            float alpha = Mathf.Lerp(1f, 0f, t / fadeTime);
            fadeImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        // 确保最终透明度为0
        fadeImage.color = new Color(0, 0, 0, 0);
    }

    private IEnumerator NowLoadingFadeImageFadeOut()
    {
        // 设置图片透明度为0
        nowLoadingFadeImage.color = new Color(0, 0, 0, 0);

        // 淡出
        float t = 0f;
        while (t < fadeTime)
        {
            t += Time.deltaTime;
            float alpha = Mathf.Lerp(1f, 0f, t / fadeTime);
            nowLoadingFadeImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        // 确保最终透明度为0
        nowLoadingFadeImage.color = new Color(0, 0, 0, 0);
        nowLoadingFadeImage.gameObject.SetActive(false);
    }

    // 淡入的方法
    private IEnumerator FadeIn()
    {
        // 设置图片透明度为1
        fadeImage.color = new Color(0, 0, 0, 1);

        // 淡入
        float t = 0f;
        while (t < fadeTime)
        {
            t += Time.deltaTime;
            float alpha = Mathf.Lerp(0f, 1f, t / fadeTime);
            fadeImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        // 确保最终透明度为1
        fadeImage.color = new Color(0, 0, 0, 1);
    }

    private IEnumerator NowLoadingFadeImageFadeIn()
    {
        // 设置图片透明度为1
        nowLoadingFadeImage.color = new Color(0, 0, 0, 1);

        // 淡入
        float t = 0f;
        while (t < fadeTime)
        {
            t += Time.deltaTime;
            float alpha = Mathf.Lerp(0f, 1f, t / fadeTime);
            nowLoadingFadeImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        // 确保最终透明度为1
        nowLoadingFadeImage.color = new Color(0, 0, 0, 1);
    }
}