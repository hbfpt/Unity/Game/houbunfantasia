using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private InputActionReference moveAction;
    private Rigidbody2D _rb;
    private Vector2 _moveInput;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();

        // 注册 InputAction 的回调方法
        moveAction.action.performed += OnMovePerformed;
        moveAction.action.canceled += OnMoveCanceled;
    }

    private void Update()
    {
        // 应用角色移动
        Vector2 targetVelocity = _moveInput * moveSpeed;
        _rb.velocity = Vector2.Lerp(_rb.velocity, targetVelocity, 0.5f);
    }

    private void OnMovePerformed(InputAction.CallbackContext context)
    {
        // 获取输入向量
        _moveInput = context.ReadValue<Vector2>();
    }

    private void OnMoveCanceled(InputAction.CallbackContext context)
    {
        // 停止角色移动
        _moveInput = Vector2.zero;
    }
}