using Newtonsoft.Json;
using System;
using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class AuthManager : MonoBehaviour
{
    private const string PRODUCT_ID = "F3EB3997";

    private const string ACTIVATE_API_PATH = "/api/activate_license";

    private const string VERIFY_API_PATH = "/api/verify_license";

    private const string CONTENT_TYPE = "application/json;charset=utf-8";

    private const string ACCEPT = "application/json";

    private const string API_KEY = "9D78A1918B0AD62FF2EE";

    private const string LANG = "chinese";

    private const string URL = "http://localhost/";

    private const string IP = "127.0.0.1";

    private const string USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; rv:47.0) Gecko/20100101 HBF/47.0";

    private static AuthManager _instance;
    private bool _status;
    public TMP_InputField _licenseInput;
    public TextMeshProUGUI _licenseInputWarn;
    public Button _startGameButton;
    public Button _conGameButton;

    public static AuthManager Instance
    {
        get { return _instance; }
    }

    public string url = "https://key.sakurakoi.top";

    private void Awake()
    {
        _instance = this;
    }

    private bool ActivateLicense(string licenseCode)
    {
        AuthData authData = new AuthData
        {
            product_id = PRODUCT_ID,
            license_code = licenseCode,
            client_name = System.Environment.MachineName,
            verify_type = "non_envato"
        };
        string json = JsonConvert.SerializeObject(authData);
        StartCoroutine(Connection(url, ACTIVATE_API_PATH, json, (result) =>
        {
            if (!result.status)
            {
                Debug.LogError("许可证无效");
                _status = false;
            }

            _status = true;
        }));
        return _status;
    }

    public void VerifyLicense(string licenseCode)
    {
        // 判断是否并没有激活码
        if (licenseCode == "0")
        {
            licenseCode = _licenseInput.text;
        }

        if (System.Text.RegularExpressions.Regex.IsMatch(licenseCode,
                @"[A-Za-z0-9]{4}\-[A-Za-z0-9]{4}\-[A-Za-z0-9]{4}\-[A-Za-z0-9]{4}"))
        {
            AuthData authData = new AuthData
            {
                product_id = PRODUCT_ID,
                license_code = licenseCode,
                client_name = System.Environment.MachineName,
                verify_type = "non_envato"
            };
            string json = JsonConvert.SerializeObject(authData);
            _licenseInputWarn.text = "";
            StartCoroutine(Connection(url, VERIFY_API_PATH, json, (result) =>
            {
                if (!result.status)
                {
                    Debug.LogError("许可证无效，尝试激活许可证");
                    _status = ActivateLicense(licenseCode);
                    _licenseInputWarn.text = "<color=yellow>许可证无效，尝试激活许可证，请等待10秒左右重试</color>";
                }
                else
                {
                    _status = true;
                    if (_status)
                    {
                        Debug.Log("yes");
                        _licenseInputWarn.text = "<color=green>激活码已验证</color>";
                        _startGameButton.interactable = true;
                        _conGameButton.interactable = true;
                        // 将激活码保存进存档
                        ES3.Save("licenseCode", licenseCode);
                    }
                    else
                    {
                        _licenseInputWarn.text = "<color=red>激活码无效/已到达使用次数</color>";
                    }
                }
            }));
        }
        else
        {
            _licenseInputWarn.text = "<color=red>激活码格式错误</color>";
        }
    }

    private IEnumerator Connection(string url, string apiPath, string form, Action<AuthJson> result)
    {
        url = url + apiPath;
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            www.SetRequestHeader("Content-Type", CONTENT_TYPE);
            www.SetRequestHeader("Accept", ACCEPT);
            www.SetRequestHeader("LB-API-KEY", API_KEY);
            www.SetRequestHeader("LB-LANG", LANG);
            www.SetRequestHeader("LB-URL", URL);
            www.SetRequestHeader("LB-IP", IP);
            www.SetRequestHeader("User-Agent", USER_AGENT);
            byte[] rawData = Encoding.UTF8.GetBytes(form);
            www.uploadHandler = new UploadHandlerRaw(rawData);
            www.downloadHandler = new DownloadHandlerBuffer();
            yield
                return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.ConnectionError ||
                www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError($"网络请求错误：{www.error}");
                result(new AuthJson
                {
                    status = false
                });
            }
            else
            {
                string responseText = www.downloadHandler.text;
                if (!string.IsNullOrEmpty(responseText))
                {
                    AuthJson responseJson = JsonConvert.DeserializeObject<AuthJson>(responseText);
                    result(responseJson);
                }
                else
                {
                    Debug.LogError("网络响应为空");
                    result(new AuthJson
                    {
                        status = false
                    });
                }
            }
        }
    }

    private void Start()
    {
        if (_startGameButton && _conGameButton)
        {
            _startGameButton.interactable = false;
            _conGameButton.interactable = false;
        }
        // 判断是否存档内有已经通过的激活码
        // 并且重新验证以防止激活码被ban
        if (ES3.KeyExists("licenseCode"))
        {
            _licenseInput.text = ES3.Load<string>("licenseCode");
            VerifyLicense(ES3.Load<string>("licenseCode"));
        }
    }
}

// json序列化
public class AuthJson
{
    public bool status { get; set; }
    public string message { get; set; }
    public string license_code { get; set; }
    public int remaining { get; set; }
    public string expire_at { get; set; }
    public string type { get; set; }
    public string activation_token { get; set; }
    public string environment { get; set; }
    public string created_at { get; set; }
    public string updated_at { get; set; }
}

public class AuthData
{
    public string product_id { get; set; }
    public string license_code { get; set; }
    public string client_name { get; set; }
    public string verify_type { get; set; }
}